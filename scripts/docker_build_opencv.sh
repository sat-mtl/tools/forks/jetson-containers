#!/usr/bin/env bash

set -e
#source scripts/docker_base.sh
BASE_IMAGE=nvcr.io/nvidia/l4t-base:r32.6.1

OPENCV_VERSION=${1:-"4.5.3"}

build_opencv()
{
	local opencv_version=$1
	local container_tag="opencv-builder:r$L4T_VERSION-cv$opencv_version"

	# opencv.csv mounts files that preclude us installing different version of opencv
	# temporarily disable the opencv.csv mounts while we build the container
	CV_CSV="/etc/nvidia-container-runtime/host-files-for-container.d/opencv.csv"
	
	if [ -f "$CV_CSV" ]; then
		sudo mv $CV_CSV $CV_CSV.backup
	fi
	
	echo "building OpenCV $opencv_version deb packages"

	sh ./scripts/docker_build.sh $container_tag Dockerfile.opencv \
			--build-arg BASE_IMAGE=$BASE_IMAGE \
			--build-arg OPENCV_VERSION=$OPENCV_VERSION

	echo "done building OpenCV $opencv_version deb packages"
	
	if [ -f "$CV_CSV.backup" ]; then
		sudo mv $CV_CSV.backup $CV_CSV
	fi
	
	# copy deb packages to jetson-containers/packages directory
	sudo docker run --rm \
	        --platform linux/arm64 \
			--volume $PWD/packages:/mount \
			$container_tag \
			cp /opencv-${opencv_version}-arm64-ubuntu18.04-cuda10.2-jetson.tar.gz /mount
			
	echo "packages are at $PWD/packages/opencv-${opencv_version}-arm64-ubuntu18.04-cuda10.2-jetson.tar.gz"
}
	
build_opencv $OPENCV_VERSION
